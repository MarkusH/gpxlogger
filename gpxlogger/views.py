from django.conf import settings
from django.http import (
    HttpResponse, HttpResponseForbidden, HttpResponseNotAllowed, JsonResponse,
)
from django.views.decorators.csrf import csrf_exempt

from .models import Log


@csrf_exempt
def log(request):
    if not request.method == 'POST':
        return HttpResponseNotAllowed(permitted_methods='POST')

    if request.POST.get('token') != settings.TOKEN:
        return HttpResponseForbidden()

    data = {}
    for key in Log.KEYS:
        val = request.POST.get(key)
        if val is not None:
            try:
                val = Log.KEY_FORMAT[key](val)
            except ValueError:
                pass
            data[key] = val

    if not data:
        return HttpResponse(status=204)

    log = Log.objects.create(data=data)

    return JsonResponse(log.as_dict())
