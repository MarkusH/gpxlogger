from django.apps import AppConfig


class GPXLoggerConfig(AppConfig):
    name = 'gpxlogger'
    verbose_name = 'GPX Logger'
