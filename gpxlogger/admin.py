import colorsys
from functools import update_wrapper

from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.template.response import TemplateResponse

from .models import Log


def get_color(value, min, max):
    if value is None or min is None or max is None:
        return '#0000ff'
    h = (value - min) / (max - min) * 120.0
    h_rel = h / 360.0
    r, g, b = colorsys.hsv_to_rgb(- h_rel + 1 / 3, 1, 1)
    color = '#%02x%02x%02x' % (int(r * 255), int(g * 255), int(b * 255))
    return color


class LogAdmin(admin.ModelAdmin):
    list_display = ('data_time', 'data_lat', 'data_lon', 'data_spd', 'data_alt')
    list_filter = ('data_prov', 'data_time')

    def geojson_view(self, request, extra_context=None):
        qs = Log.objects.has_data().only_gps()

        try:
            start = forms.DateTimeField().to_python(request.GET.get('start', ''))
            qs = qs.show_from(start)
        except ValidationError:
            pass

        try:
            end = forms.DateTimeField().to_python(request.GET.get('end', ''))
            qs = qs.show_from(end)
        except ValidationError:
            pass

        qs = qs.order_by('data_time')
        format = request.GET.get('format')
        if format == 'points':
            resp = {
                'type': 'FeatureCollection',
                'features': [
                    {
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Point',
                            'coordinates': (l.data_lon, l.data_lat),
                        },
                        'properties': {
                            'id': l.id,
                            'data': l.data,
                        }
                    }
                    for l in qs[:100]
                ]
            }
        else:
            coordinates = list(qs)
            features = []
            resp = {
                'type': 'FeatureCollection',
                'features': features,
            }
            alt_min, alt_max = None, None
            spd_min, spd_max = None, None
            for last, this in zip(coordinates, coordinates[1:]):
                if alt_min is None:
                    alt_min = this.data_alt
                elif this.data_alt is not None:
                    alt_min = min(alt_min, this.data_alt)
                if alt_max is None:
                    alt_max = this.data_alt
                elif this.data_alt is not None:
                    alt_max = max(alt_max, this.data_alt)
                if spd_min is None:
                    spd_min = this.data_spd
                elif this.data_spd is not None:
                    spd_min = min(spd_min, this.data_spd)
                if spd_max is None:
                    spd_max = this.data_spd
                elif this.data_spd is not None:
                    spd_max = max(spd_max, this.data_spd)

            for last, this in zip(coordinates, coordinates[1:]):
                features.append({
                    'type': 'Feature',
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': [
                            (last.data_lon, last.data_lat),
                            (this.data_lon, this.data_lat),
                        ],
                    },
                    'properties': {
                        'color': get_color(this.data_spd, spd_min, spd_max),
                    }
                })

        return JsonResponse(resp)

    def map_view(self, request):
        opts = self.model._meta
        app_label = opts.app_label
        context = dict(
            self.admin_site.each_context(request),
            module_name=str(opts.verbose_name_plural),
            preserved_filters=self.get_preserved_filters(request),
            title='Map',
            opts=opts,
            app_label=app_label,
            range_start=request.GET.get('start'),
            range_end=request.GET.get('end'),
            format=request.GET.get('format'),
        )
        return TemplateResponse(request, 'admin/gpxlogger/map_view.html', context)

    def get_urls(self):
        from django.urls import path

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        urlpatterns = [
            path('geojson/', wrap(self.geojson_view), name='%s_%s_geojson' % info),
            path('map/', wrap(self.map_view), name='%s_%s_map' % info),
        ]
        urlpatterns += super().get_urls()
        return urlpatterns


admin.site.register(Log, LogAdmin)
