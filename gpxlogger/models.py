from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.indexes import GinIndex
from django.db import models


class LogQuerySet(models.QuerySet):

    def has_data(self):
        return self.filter(data_lat__isnull=False, data_lon__isnull=False)

    def only_gps(self):
        return self.filter(data_prov='gps')

    def show_from(self, start):
        if start:
            return self.filter(data_time__gte=start)
        return self

    def show_to(self, end):
        if end:
            return self.filter(data_time__lte=end)
        return self


class Log(models.Model):
    data = JSONField()
    data_acc = models.FloatField('Accuracy', null=True, editable=False)
    data_act = models.TextField('Activity', null=True, editable=False)
    data_aid = models.TextField('Android ID', null=True, editable=False)
    data_alt = models.FloatField('Altitude', null=True, editable=False)
    data_batt = models.FloatField('Battery', null=True, editable=False)
    data_desc = models.TextField('Annotation', null=True, editable=False)
    data_dir = models.TextField('Direction', null=True, editable=False)
    data_filename = models.TextField('Filename', null=True, editable=False)
    data_lat = models.FloatField('Latitude', null=True, editable=False)
    data_lon = models.FloatField('Longitude', null=True, editable=False)
    data_prov = models.TextField('Provider', null=True, editable=False)
    data_sat = models.IntegerField('Satellites', null=True, editable=False)
    data_ser = models.TextField('Serial', null=True, editable=False)
    data_spd = models.FloatField('Speed', null=True, editable=False)
    data_starttimestamp = models.IntegerField('Start Timestamp', null=True, editable=False)
    data_time = models.DateTimeField('Time', null=True, editable=False)
    data_timestamp = models.IntegerField('Timestamp', null=True, editable=False)

    objects = LogQuerySet.as_manager()

    KEYS = {
        'acc',
        'act',
        'aid',
        'alt',
        'batt',
        'desc',
        'dir',
        'filename',
        'lat',
        'lon',
        'prov',
        'sat',
        'ser',
        'spd',
        'starttimestamp',
        'time',
        'timestamp',
    }
    KEY_FORMAT = {
        'acc': float,
        'act': str,
        'aid': str,
        'alt': float,
        'batt': float,
        'desc': str,
        'dir': str,
        'filename': str,
        'lat': float,
        'lon': float,
        'prov': str,
        'sat': int,
        'ser': str,
        'spd': float,
        'starttimestamp': int,
        'time': str,
        'timestamp': int,
    }

    class Meta:
        indexes = [
            GinIndex(fields=['data'], name='data_gin_idx'),
        ]

    def save(self, *args, **kwargs):
        for key in self.KEYS:
            setattr(self, 'data_%s' % key, self.data.get(key))
        super().save(*args, **kwargs)

    def as_dict(self):
        return {
            'id': self.pk,
            'data': self.data,
        }
