# Generated by Django 2.0.1 on 2018-01-11 16:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gpxlogger', '0004_auto_20180111_1644'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='log',
            name='data_time',
        ),
        migrations.AddField(
            model_name='log',
            name='data_time',
            field=models.DateTimeField(editable=False, null=True),
        ),
    ]
