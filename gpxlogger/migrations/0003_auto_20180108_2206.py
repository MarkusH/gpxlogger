# Generated by Django 2.0.1 on 2018-01-08 22:06

import django.contrib.postgres.indexes
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gpxlogger', '0002_format_data'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='log',
            index=django.contrib.postgres.indexes.GinIndex(fields=['data'], name='data_gin_idx'),
        ),
    ]
